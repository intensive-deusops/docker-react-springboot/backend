FROM maven:3.6.0-jdk-11-slim AS builder

COPY src /app/src
COPY pom.xml /app

ARG DB_HOST=mariadb
ARG DB_PORT=3306
ARG DB_DATABASE=fullstack
ARG DB_USER=user
ARG DB_PASS=pass

WORKDIR /app

RUN set -eux; \
    sed -i  "s|^spring.datasource.url=.*|spring.datasource.url=jdbc:mysql://${DB_HOST}:${DB_PORT}/${DB_DATABASE}|" /app/src/main/resources/application.properties; \
    sed -i  "s|^spring.datasource.username=.*|spring.datasource.username=${DB_USER}|" /app/src/main/resources/application.properties; \
    sed -i  "s|^spring.datasource.password=.*|spring.datasource.password=${DB_PASS}|" /app/src/main/resources/application.properties; \
    mvn -f /app/pom.xml clean package -Dmaven.test.skip=true


FROM openjdk:11-jre-slim
COPY --from=builder /app/target/studentsystem-0.0.1-SNAPSHOT.jar  /app/studentsystem.jar
WORKDIR /app
EXPOSE 8080
CMD ["java","-jar","studentsystem.jar"]
